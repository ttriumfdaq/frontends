/****************************************************************
 * $Id: e614db.h,v 1.2 2005/08/04 00:29:34 twistonl Exp $
 * 
 * See pqlib documentation for description of PQ..() functions.
 * http://www.ca.postgresql.org/devel-corner/docs/postgres/libpq.html
 * 
 * To forward the Postgres TCP/IP port do from your local machine:
 * 
 * 	ssh -L 3333:e614db.triumf.ca:5432 e614db.triumf.ca
 *
 * After this you should be able to connect to your local port 3333
 * using e614db_connect(), as long as the ssh session is running. 
 * 3333 can be number of any local unused port > 1024
 *
 * A.Gaponenko, June 2001
 */

#ifndef E614DB_H
#define E614DB_H

#include <time.h>

#include "libpq-fe.h"

#ifdef __cplusplus
extern              "C"
{
#endif /* __cplusplus */


  /*================================================================
   * When dbname is NULL a connection to the current production
   * version of the TWIST hardware database will be made. Non-null
   * dbname requests a connection to a particular database.
   * e614db_connect() always tries to connect to the given TCP/IP port
   * on localhost.  In other respects it acts like PQconnectdb().
   * 
   * Returns NULL if dbname is not given and query for the current 
   * production version is failed for any reason.
   * Non NULL return value still has to be checked with PQstatus()
   * before use.
   * 
   * Note that PQfinish() should be called on any non-NULL return value
   * to do clean up even if the connection attempt has failed.  
   */

  PGconn *e614db_connect(const char* host,
			 int port,
			 const char* username,
			 const char* password,
			 const char* dbname);

  /* The same as above, with hardcoded args for read-only connection
   * to the current production database
   */

  PGconn *e614db_default_connect();

  /*================================================================
   * Close a connection open wiht e614db_connect(). 
   * Currently just a wrapper around PQfinish() 
   */

  void e614db_close(PGconn *conn);

  /*================================================================
   * Query the database for calibration parameters valid at the given time.
   * On success return 0 and set parameter values.
   * Otherwise parameters are left intact.
   * A negative return value indicates a query execution problem.
   * A positive return value means requested parameters are not available.
   */

  int e614db_get_PostAmp_calib(PGconn *conn,
			       int sernum, time_t workdate,
			       double *A, double *B );

  /* chi2 may be set to 0 if not known */
  int e614db_get_Therm_calib(PGconn *conn,
			     int sernum, time_t workdate,
			     double *M, double *dM,
			     double *B, double *dB,
			     double *chi2);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* E614DB_H */
