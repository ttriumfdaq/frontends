/*
 * Name: serial.c
 * Description: serial port interface
 * Author: Peter Green, K.Olchanski
 *
 * $Id: serial.c,v 1.5 2007/05/12 23:25:05 twistonl Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>

#include "serial.h"
#include "midas.h"

extern char *frontend_name;

int serial_trace = 0;

static int getaddr (const char *s) {
  /* Get (numeric) Internet address, given either machine name
   * or 'dotted' form
   * Returned address is 0 for an error (e.g. gethostbyname fails) 
   */

  struct hostent *ph;
  int inet [4];
  int iaddr;
  int i;

  if (isdigit (*s)) {
    sscanf (s, "%d.%d.%d.%d", &inet[0], &inet[1], &inet[2], &inet[3]);
    for (i=0; i<4; ++i) {
      iaddr <<= 8;
      iaddr += inet[i];
    }
    /* Get the "ended-ness" right!
     */
    iaddr = htonl (iaddr);
  } else {
    if ((ph = gethostbyname (s)) == NULL)  return (0);
    bcopy (ph->h_addr, (char *) &iaddr, 4);
  }
  return (iaddr);
}

int serial_open(const char *s)
{
  /* Open (terminal or telnet) device for HPDVM
   *
   * Hardware (terminal) driver is assumed set up already, with a command like
   *
   * stty -F /dev/cua0 raw -echo crtscts 38400
   *
   * to set rts/cts flow control and 38400 baud.
   *
   * These settings MUST be compatible with what is set in the device itself
   * (via front panel config) or (of course) it won't work
   *
   * The name (*s) is one of:
   * (1) /dev/ttySn - use hardware com port
   * (2) port@xww.xx.yy.zz - open a telnet socket (for remote terminal server)
   */

  int port;
  int inetaddr, len;
  struct sockaddr_in *inaddr;
  int flags;
  char buff [256];
  int hpfd;

  if (isdigit (*s)) {
    /* Looks like a TCP socket is needed here
     */
    sscanf (s, "%d@%s", &port, buff);
    printf("serial_open: Connecting to %s port %d\n",buff,port);

    hpfd = socket (AF_INET, SOCK_STREAM, 0);
    if (hpfd < 0) {
      cm_msg (MERROR, frontend_name, "Cannot connect to %s: socket(AF_INET,SOCK_STREAM,0) error %d (%s)", s, errno, strerror(errno));
      return -1;
    }
    inaddr = (struct sockaddr_in *) malloc (sizeof (struct sockaddr_in)); /* memory leak */
    bzero ((char *) inaddr, sizeof (struct sockaddr_in));
    inaddr->sin_family = AF_INET;
    inaddr->sin_port = htons (port);
    
    inetaddr = getaddr (buff);
    if (inetaddr == 0) {
      cm_msg (MERROR, frontend_name, "Cannot get IP address for %s", s);
      return -1;
    }

    bcopy (&inetaddr, (char *) &inaddr->sin_addr, 4);
    len = sizeof (struct sockaddr_in);
    if (connect (hpfd, (void*)inaddr, len) < 0) {
      cm_msg (MERROR, frontend_name, "Cannot connect to %s: connect() error %d (%s)", s, errno, strerror(errno));
      return -1;
    }
  } else {
    /* Guess I'm just a serial port
     */
    printf("serial_open: Open serial port %s\n", s);
    hpfd = open(s,O_RDWR);
    if (hpfd < 0) {
      cm_msg (MERROR, frontend_name, "Cannot open serial port %s: open() error %d (%s)", s, errno, strerror(errno));
      return -1;
    }
  }
  /* We run this thing in NO-DELAY mode, so that if anything happens, we
   * don't hang the entire front-end.
   */
  flags = fcntl (hpfd, F_GETFL, 0);
  flags |= O_NDELAY;
  if (fcntl (hpfd, F_SETFL, flags) < 0) {
    cm_msg (MERROR, frontend_name, "serial_open: fcntl(F_SETFL,O_NDELAY) error %d (%s)", errno, strerror(errno));
    return -1;
  }

  return hpfd;
}

int serial_close(int fd)
{
  if (close(fd)<0) {
    cm_msg (MERROR, frontend_name, "serial_close: close() error %d (%s)", errno, strerror(errno));
    return -1;
  }
  return 0;
}

int serial_write(int fd,const char *s)
{
  int len;

  if (serial_trace)
    printf("serial_write: [%s]\n",s);

  /* Write string to the device */
  assert(fd > 0);

  len = strlen(s);
  while (len > 0)
    {
      int status = write(fd, s, len);
      if (status == -1 && errno == EAGAIN)
	continue;
      if (status != len)
	{
	  cm_msg (MERROR, frontend_name, "serial_write: write() error %d (%s)", errno, strerror(errno));
	  return -1;
	}
      break;
    }

  return 0;
}

int serial_writeline(int fd,const char *s)
{
  char buf[256];
  snprintf(buf,sizeof(buf)-1,"%s\n",s);
  return serial_write(fd,buf);
}

int serial_readline(int fd,char *s,int maxlen,int timeout)
{
  /* Read response from the DVM into buffer
   * Read until LF or maxlen characters 
   * Waits at most timeout msec before declaring an error (0 == no timeout)
   */

  DWORD milli_start = ss_millitime();
  int nflush = 0;
  const char* inputbuf = s;
  
  assert(fd > 0);

  while (1)
    {
      int rd = read(fd, s, 1);

      if (rd == 0)
	{
	  cm_msg (MERROR, frontend_name, "serial_readline: EOF");
	  return -1;
	}

      //printf("serial_readline: rd %d, data 0x%x [%c]\n",rd,*s,*s);
      
      if (rd != 1)
	{
	  if (errno == EAGAIN)
	    {
	      if (timeout < 0)
		{
		  if (s==inputbuf)
		    {
		      if (serial_trace)
			printf("serial_readline: no data\n");
		      
		      return 0;
		    }

		  timeout = -timeout;
		}

	      if ((ss_millitime () - milli_start) > timeout)
		{
		  if (serial_trace)
		    printf("serial_readline: timeout, return -2\n");
		  //cm_msg (MERROR, frontend_name, "serial_readline: timeout");
		  *s = 0;
		  return -2;
		}

	      ss_sleep (10);
	      continue;
	    }

	  cm_msg (MERROR, frontend_name, "serial_readline: read() error %d (%s)", errno, strerror(errno));
	  return -1;
	}
      
      /* Telnet connections have these extra character sequences every now and then */
      
      if (nflush > 0)
	{
	  --nflush;
	  continue;
	}

      if (*s == (char)255)
	{
	  nflush = 2;
	  continue;
	}

      if (*s == '\r') continue;
      if (*s == '\n') break;
      if (!isprint(*s)) continue;
      if (--maxlen <= 0) break;
      s++;
    }

  *s = '\0';

  if (serial_trace)
    printf("serial_readline: return [%s]\n",inputbuf);

  return (s - inputbuf);
}

int serial_flush(int fd)
{
  char buf[128];
  int count = 0;
  int i;
  for (i=0; i<1000; i++)
    {
      int rd = read(fd,buf,sizeof(buf));
      if (rd > 0)
	{
	  count += rd;
	  continue;
	}

      if (rd == 0)
	{
	  cm_msg(MERROR, frontend_name, "serial_flush: unexpected EOF");
	  return -1;
	}

      // we are in the NDELAY mode
      if (errno == EAGAIN)
	{
	  printf("serial_flush: Flushed %d bytes\n",count);
	  return 0;
	}

      cm_msg(MERROR, frontend_name, "serial_flush: read() error %d (%s)", errno, strerror(errno));
      return -1;
    }

  cm_msg(MERROR, frontend_name, "serial_flush: the serial line is babbling");
  return -1;
}

/* end file */
