/*
 * Name: serial.h
 * Description: serial port interface
 *
 * $Id: serial.h,v 1.3 2004/08/05 21:02:52 twistonl Exp $
 */

/* returns fd on success, -1 on failure */
int serial_open(const char *s);

/* returns 0 on success, -1 on failure */
int serial_close(int fd);

/* returns 0 on success, -1 on failure */
int serial_write(int fd,const char *s);

/* returns 0 on success, -1 on failure */
int serial_writeline(int fd,const char *s);

/* returns strlen(s) on success, -1 on failure */
int serial_readline(int fd,char *s,int maxlen,int mtimeout);

/* returns 0 on success, -1 on failure */
int serial_flush(int fd);

/* enable/disable tracking of all serial input and output activity */
extern int serial_trace;

/* end file */
