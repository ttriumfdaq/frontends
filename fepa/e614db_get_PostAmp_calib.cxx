/****************************************************************
 * $Id: e614db_get_PostAmp_calib.c,v 1.3 2002/10/18 02:02:23 olchansk Exp $
 */

#include <time.h>
#include <stdio.h>

#include "libpq-fe.h"

#include "e614db.h"

/*================================================================*/
int e614db_get_PostAmp_calib(PGconn *conn, 
			     int sernum, time_t workdate,
			     double *A, double *B )
{
  char querybuf[256];
  PGresult *qq;
  int ret = -1;
  
  /* prepare SQL query */
  sprintf(querybuf, "SELECT a,b FROM postamp_pars"
	  " WHERE sernum=%d AND workdate <= '%s'"
	  " ORDER BY workdate DESC LIMIT 1;",
	  sernum, ctime(&workdate)
	  );
  
  /* and execute it */
  qq = PQexec(conn, querybuf);
  
  /* check result status */
  if(PQresultStatus(qq) == PGRES_TUPLES_OK) {
    ret = 1; /* Query executed */

    /****************************************************************/
#if 0 
  {
    PQprintOpt po;
    po.header = 1;      /* print output field headings and row count */
    po.align = 1;       /* fill align the fields */
    po.standard = 0;    /* old brain dead format */
    po.html3 = 0;       /* output html tables */
    po.expanded = 0;    /* expand tables */
    po.pager = 0;       /* use pager for output if needed */
    po.fieldSep = "\t|";   /* field separator */
    po.tableOpt = "";   /* insert to HTML table ... */
    po.caption = "";    /* HTML caption */
    po.fieldName = NULL; /* null terminated array of replacement field names */
    
    PQprint(stdout, qq, &po);
  }
#endif /* 0 */
  /****************************************************************/

    if(PQntuples(qq) == 1) {
      double tmpa, tmpb;
      if((1 == sscanf(PQgetvalue(qq, 0, 0), "%lg", &tmpa))
	 &&(1 == sscanf(PQgetvalue(qq, 0, 1), "%lg", &tmpb))
	 )
	{
	  *A = tmpa;
	  *B = tmpb;
	  ret = 0;
	}
    }
  }
  
  PQclear(qq);

  return ret;
}
/*============================ EOF ===============================*/
