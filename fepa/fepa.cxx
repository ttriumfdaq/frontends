/*******************************************************************\

  Name:         fepa.c
  Created by:   K.Olchanski

  Contents:     Front end for Post Amps

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <ctype.h>
#include <assert.h>

#include "midas.h"
#include "msystem.h"
#include "e614.err"
#include "serial.h"

#define EQ_NAME "PostAmp"

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "PostAmp";

/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
//INT display_period = 3000;
INT display_period = 0;

/* maximum event size produced by this frontend */
INT max_event_size = 200;
INT max_event_size_frag = 5*1024*1024;

/* buffer size to hold events */
INT event_buffer_size = 300*200;

/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

/* E614 Specific things go here
 * The first bunch need to be in ALL Slow Controls Front Ends
 */

HNDLE hDB;
INT gbl_run_number;

/* From here on are things which are specific to each FrontEnd
 */

/* Root of Equipment/Settings tree */
HNDLE hSettingRoot;

/* Roots of Equipment/Variables tree */
HNDLE hVarRoot;

/* buffer to contain Command from hotlink */
#define CMDSIZE 32
char cmdbuff [CMDSIZE];

/* PostAmp specific variables
 */

HNDLE hDVTp;
HNDLE hDThresh;
HNDLE hDTPEnable;
HNDLE hMTPEnable;

INT Period;
float VTPSensitivity;
float VThreshSensitivity;
float TempSensitivity;
float TempAlarm;
float TempAlarmPACT;
float VoltSensitivity;

/* Allow for 12 Crates (not necessarily all full)
 */
#define NUMPACRATES    2
#define SLOTSPERCRATE 24
  //#define SLOTSPERCRATE 8
#define NUMPACHANS (NUMPACRATES*SLOTSPERCRATE)

DWORD NumChan;
int NumCrate;

#define WRITE_VTH 0x0001
#define WRITE_VTP 0x0002
#define WRITE_TPE 0x0004

#define WRITE_ALL (WRITE_VTH|WRITE_VTP|WRITE_TPE)

int doWriteCrate[NUMPACHANS];
int doWriteChannel[NUMPACHANS];

#define READ_VTHA   1
#define READ_VTHB   2
#define READ_VTP    3
#define READ_TEMP   4
#define READ_VOLT   5

#define MAX_REQ 5

int    channelTimeoutReq[NUMPACHANS];
time_t channelTimeoutTime[NUMPACHANS];

BOOL D_TPEnable[NUMPACRATES];
BOOL M_TPEnable[NUMPACRATES];

#define kMaxChains 4

int gNumChains = 0;
int fd[kMaxChains];
int inflight[kMaxChains];

int gCycleTime = 9999; // time to read all PostAmp channels

#include "scdata.h"

float D_VTp[NUMPACHANS];
float D_Thresh[NUMPACHANS];

ScData_t *vtpData;
ScData_t *thraData;
ScData_t *thrbData;
ScData_t *tempData;
ScData_t *vplusData;
ScData_t *vminusData;

time_t thraTime[NUMPACHANS];
time_t thrbTime[NUMPACHANS];
time_t vtpTime[NUMPACHANS];
time_t tempTime[NUMPACHANS];
time_t voltageTime[NUMPACHANS];

time_t maxtempTime[NUMPACRATES];
time_t tpeTime[NUMPACRATES];

INT PACtrlSerialNo[NUMPACRATES];
INT PASerialNo[NUMPACHANS];
float ThreshCaliA[NUMPACHANS];
float ThreshCaliB[NUMPACHANS];

ScData_t *scPAStatus;
ScData_t *scPACtrlStatus;

ScData_t *scTPEnableStatus;
ScData_t *maxtempData;

#define PA_missing(i) ((i >= NumChan)||(PASerialNo[i]<=0))
#define PACtrl_missing(i) ((i>=NumCrate)||(PACtrlSerialNo[i]<=0))
  
/*-- E614 Function declarations -----------------------------------------*/

INT read_PA_Event(char *pevent, int off);

void docmd (HNDLE hDB, HNDLE hKey, void *info);
INT config (void);
INT get_settings();

/*-- Equipment list ------------------------------------------------*/

#define EVID_PA 32

EQUIPMENT equipment[] = {

  { EQ_NAME,         /* equipment name */
    {
      EVID_PA, 0,           /* event ID, trigger mask */
      "SYSTEM",             /* event buffer */
      EQ_PERIODIC,          /* equipment type */
      0,                    /* event source */
      "MIDAS",              /* format */
      TRUE,                 /* enabled */
      RO_RUNNING|RO_STOPPED|RO_PAUSED,  /* read in all states */
      30,                   /* poll every so milliseconds */
      0,                    /* stop run after this event limit */
      0,                    /* number of sub events */
      60,                   /* do log history at most once per minute */
      "", "", ""
    },
    read_PA_Event,/* readout routine */
  },
  { "" }
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.
  
  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send 
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>

int serial_trace = 0;

int serial_open(const char *addr)
{
  char laddr[256];
  strlcpy(laddr, addr, sizeof(laddr));
  char* s = strchr(laddr,':');
  if (!s)
    {
      cm_msg (MERROR, frontend_name, "serial_open: Invalid address \'%s\': no \':\', should look like \'hostname:tcpport\'", laddr);
      return -1;
    }

  *s = 0;

  int port = atoi(s+1);
  if (port == 0)
    {
      cm_msg (MERROR, frontend_name, "serial_open: Invalid address: \'%s\', tcp port number is zero", laddr);
      return -1;
    }

  struct hostent *ph = gethostbyname(laddr);
  if (ph == NULL)
    {
      cm_msg (MERROR, frontend_name, "serial_open: Cannot find ip address for \'%s\', errno %d (%s)", laddr, h_errno, hstrerror(h_errno));
      return -1;
    }

  int fd = socket (AF_INET, SOCK_STREAM, 0);
  if (fd < 0)
    {
      cm_msg (MERROR, frontend_name, "serial_open: Cannot create tcp socket, errno %d (%s)", errno, strerror(errno));
      return -1;
    }

  struct sockaddr_in inaddr;

  memset(&inaddr, 0, sizeof(inaddr));
  inaddr.sin_family = AF_INET;
  inaddr.sin_port = htons(port);
  memcpy((char *) &inaddr.sin_addr, ph->h_addr, 4);
  
  cm_msg(MINFO, frontend_name, "serial_open: Connecting to %s port %d", laddr, port);
  
  int status = connect(fd, (sockaddr*)&inaddr, sizeof(inaddr));
  if (status == -1)
    {
      cm_msg (MERROR, frontend_name, "serial_open: Cannot connect to %s:%d, connect() error %d (%s)", laddr, port, errno, strerror(errno));
      return -1;
    }

  int flags = fcntl(fd, F_GETFL, 0);
  flags |= O_NDELAY;

  status = fcntl (fd, F_SETFL, flags);
  if (status == -1)
    {
      cm_msg (MERROR, frontend_name, "serial_open: Cannot set no-delay mode, fcntl(F_SETFL,O_NDELAY) error %d (%s)", errno, strerror(errno));
      return -1;
    }

  return fd;
}

int serial_close(int fd)
{
  int status = close(fd);
  if (status == -1)
    {
      cm_msg (MERROR, frontend_name, "serial_close: close() error %d (%s)", errno, strerror(errno));
      return -1;
    }

  return 0;
}

int serial_write(int fd, const char *s)
{
  if (serial_trace)
    printf("serial_write: [%s]\n",s);

  /* Write string to the device */
  assert(fd > 0);

  int len = strlen(s);
  while (len > 0)
    {
      int status = write(fd, s, len);
      if (status == -1 && errno == EAGAIN)
	continue;
      if (status != len)
	{
	  cm_msg (MERROR, frontend_name, "serial_write: write() error %d (%s)", errno, strerror(errno));
	  return -1;
	}
      break;
    }

  return 0;
}

int serial_writeline(int fd, const char *s)
{
  char buf[256];
  snprintf(buf, sizeof(buf)-1, "%s\n", s);
  return serial_write(fd, buf);
}

int serial_readline(int fd, char *s, int maxlen, int timeout)
{
  /* Read response from the DVM into buffer
   * Read until LF or maxlen characters 
   * Waits at most timeout msec before declaring an error (0 == no timeout)
   */

  DWORD milli_start = ss_millitime();
  int nflush = 0;
  const char* inputbuf = s;
  
  assert(fd > 0);

  while (1)
    {
      int rd = read(fd, s, 1);

      if (rd == 0)
	{
	  cm_msg (MERROR, frontend_name, "serial_readline: EOF");
	  return -1;
	}

      //printf("serial_readline: rd %d, data 0x%x [%c]\n",rd,*s,*s);
      
      if (rd != 1)
	{
	  if (errno == EAGAIN)
	    {
	      if (timeout < 0)
		{
		  if (s==inputbuf)
		    {
		      if (serial_trace > 1)
			printf("serial_readline: no data\n");
		      
		      return 0;
		    }

		  timeout = -timeout;
		}

	      if ((ss_millitime () - milli_start) > timeout)
		{
		  if (serial_trace)
		    printf("serial_readline: timeout, return -2\n");
		  //cm_msg (MERROR, frontend_name, "serial_readline: timeout");
		  *s = 0;
		  return -2;
		}

	      ss_sleep (10);
	      continue;
	    }

	  cm_msg (MERROR, frontend_name, "serial_readline: read() error %d (%s)", errno, strerror(errno));
	  return -1;
	}
      
      /* Telnet connections have these extra character sequences every now and then */
      
      if (nflush > 0)
	{
	  --nflush;
	  continue;
	}

      if (*s == (char)255)
	{
	  nflush = 2;
	  continue;
	}

      if (*s == '\r') continue;
      if (*s == '\n') break;
      if (!isprint(*s)) continue;
      if (--maxlen <= 0) break;
      s++;
    }

  *s = '\0';

  if (serial_trace)
    printf("serial_readline: return [%s]\n",inputbuf);

  return (s - inputbuf);
}

int serial_flush(int fd)
{
  char buf[128];
  int count = 0;
  int i;
  for (i=0; i<1000; i++)
    {
      int rd = read(fd,buf,sizeof(buf));
      if (rd > 0)
	{
	  count += rd;
	  continue;
	}

      if (rd == 0)
	{
	  cm_msg(MERROR, frontend_name, "serial_flush: unexpected EOF");
	  return -1;
	}

      // we are in the NDELAY mode
      if (errno == EAGAIN)
	{
	  printf("serial_flush: Flushed %d bytes\n",count);
	  return 0;
	}

      cm_msg(MERROR, frontend_name, "serial_flush: read() error %d (%s)", errno, strerror(errno));
      return -1;
    }

  cm_msg(MERROR, frontend_name, "serial_flush: the serial line is babbling");
  return -1;
}

INT my_find_and_load (HNDLE hDB, HNDLE hRoot, const char *name, HNDLE *hKey, void *data, int numvalues, int type)
{
  /* function to locate the Key for data and load it from the odb
   * hKey may be null if the key is not required
   * Returns SUCCESS if successful
   */
  HNDLE hTemp;
  int size;
  int status;

  //printf("my_find_and_load: %d %d %s[%d] of type %d\n", hDB, hRoot, name, numvalues, type);
  
  status = db_find_key (hDB, hRoot, (char*)name, &hTemp);
  if (status != SUCCESS)
    {
      cm_msg (MERROR, frontend_name, "Cannot find \'%s\' (array %d of type %d), error %d", name, numvalues, type, status);
      return status;
    }

  if (type == TID_BYTE || type == TID_SBYTE || type == TID_CHAR || type == TID_STRING)
    size = numvalues;
  else if (type == TID_WORD || type == TID_SHORT)
    size = numvalues * sizeof (short);
  else if (type == TID_DWORD || type == TID_INT
	   || type == TID_BOOL  || type == TID_FLOAT)
    size = numvalues * sizeof (DWORD);
  else if (type == TID_DOUBLE)
    size = numvalues * sizeof (double);
  else
    {
      cm_msg (MERROR, frontend_name, "Key \'%s\' unknown type %d", name, type);
      return FE_ERR_ODB;
    }

  status = db_get_data (hDB, hTemp, data, &size, type);
  if (status != SUCCESS)
    {
      cm_msg (MERROR, frontend_name, "Cannot read value of \'%s\', error %d", name, status);
      return status;
    }

  if (hKey != NULL)
    *hKey = hTemp;

  return SUCCESS;
}

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
  HNDLE hKey;
  KEY keyinfo;
  int status;
  int size;
  int i;

  /* Set root of directory tree
   * (Note: hDB comes from the caller - mfe.c)
   */

  //cm_set_watchdog_params (FALSE, 0);

  /* Renee would like me to get the run number correct right from the start */
  status = my_find_and_load(hDB, 0, "/Runinfo/Run number", NULL, &gbl_run_number, 1, TID_INT);

  /* Get handles to various points in the ODB tree
   */
  sprintf (cmdbuff, "/Equipment/%s/Settings", equipment[0].name);
  status = db_find_key(hDB, 0, cmdbuff, &hSettingRoot);
  if (status != SUCCESS) {
    return (status);
  }
  sprintf (cmdbuff, "/Equipment/%s/Variables", equipment[0].name);
  status = db_find_key(hDB, 0, cmdbuff, &hVarRoot);
  if (status != SUCCESS) {
    return (status);
  }

  /* Open the Device(s)
   */
  status = db_find_key(hDB, hSettingRoot, "Device", &hKey);
  if (status != SUCCESS) {
    return (status);
  }
  status = db_get_key (hDB, hKey, &keyinfo);
  if (status != SUCCESS) {
    cm_msg (MERROR, "frontend_init", "Can't get Device key %d", status);
    return (status);
  }
  if (keyinfo.num_values < 2) {
    cm_msg (MERROR, "frontend_init", "Too few Devices %d - min is %d",keyinfo.num_values, 2);
    return (FE_ERR_ODB);
  }

  set_equipment_status(EQ_NAME, "Connecting to PostAmp", "#00FF00");

  for (i=0; i<4; i++) {
    char name[256];
    size = sizeof(name);
    status = db_get_data_index (hDB, hKey, name, &size, i, TID_STRING);
    if (status != SUCCESS) {
      cm_msg (MERROR, "config", "Cannot read device name %d, error %d", i, status);
      return (status);
    }

    printf("name[%d] is \'%s\'\n", i, name);

    if (name[0] == '\0') break;
    if (1)
      {
	switch (i)
	  {
	  default:
	    fd[i] = serial_open(name);
	    break;
#if 0
	  case 0:
	    fd[i] = serial_open("/dev/ttyUSB9");
	    break;
	  case 1:
	    //fd[i] = serial_open("3006@192.168.1.4");
	    fd[i] = serial_open("4007@127.0.0.1");
	    break;
#endif
	  }
      }
    else
      {
	sprintf(name,"/dev/ttyUSB%d",5+i);
	fd[i] = serial_open(name);
      }
    if (fd[i] < 0) {
      cm_msg (MERROR, "frontend_init", "Cannot open device %s, serial_open() returned %d", name, fd[i]);
      return (FE_ERR_HW);
    }
    gNumChains = i+1;
  }

  printf("Connected to %d serial interfaces\n",gNumChains);

  set_equipment_status(EQ_NAME, "Connected to PostAmp", "#00FF00");

  /* Set up hot-link to the Command string
   */
  status = db_find_key(hDB, hSettingRoot, "Command", &hKey);
  if (status != SUCCESS) {
    return (status);
  }
  status = db_open_record (hDB, hKey, cmdbuff, sizeof (cmdbuff), 
			   MODE_READ, docmd, NULL);
  if (status != SUCCESS) {
    cm_msg (MERROR, "frontend_init", "Can't open hotlink for /Equipment/%s/Settings/Command key",
	    equipment[0].name);
    return (status);
  }

  status = config ();
  if (status != SUCCESS) {
    cm_msg (MERROR, "frontend_init", "Error configuring /Equipment/%s/Settings/Device",
	    equipment[0].name);
    return (status);
  }

  set_equipment_status(EQ_NAME, "frontend_init Ok", "#00FF00");

  return (status);
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  gbl_run_number = run_number;
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  ss_sleep(200);
  return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\
  
  Readout routines for different events

\********************************************************************/

extern "C" INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  if (test) {
    ss_sleep (count);
  }
  return (0);
}

/*-- Interrupt configuration ---------------------------------------*/

extern "C" INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
  switch(cmd)
    {
    case CMD_INTERRUPT_ENABLE:
      break;
    case CMD_INTERRUPT_DISABLE:
      break;
    case CMD_INTERRUPT_ATTACH:
      break;
    case CMD_INTERRUPT_DETACH:
      break;
    }
  return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

int get_pa_chan(int crate,int module)
{
  int chan;

  if (module<1)
    return -1;
  if (module>SLOTSPERCRATE)
    return -1;
  if (crate<0)
    return -1;

  chan = crate*SLOTSPERCRATE + module - 1;

  if (chan<0 || chan>=NUMPACHANS)
    return -1;
  
  return chan;
}

int findchain(int crate)
{
  switch (crate)
    {
    case  0: return 0;
    case  1: return 1;
    case  2: return 0;
    case  3: return 1;
    case  4: return 0;
    case  5: return 1;
    case  6: return 2;
    case  7: return 3;
    case  8: return 2;
    case  9: return 3;
    case 10: return 2;
    case 11: return 3;
    }

  //return crate%2;
  printf("findchain: Invalid crate number %d\n",crate);
  abort();
}

void writeToCrate(int crate,const char*data)
{
  int chain = findchain(crate);
  int status = serial_write(fd[chain],data);
  if (status != 0)
    {
      printf("Cannot write to chain %d, crate %d, status %d, data [%s]\n",chain,crate,status,data);
    }

  //printf("write to crate %d, chain %d, fd %d: %s", crate, chain, fd[chain], data);
  inflight[chain]++;
  //usleep(5000);
}

void request_vtha(int crate, int module)
{
  char sbuff[256];
  
  /* Request threshold A voltage */
  sprintf (sbuff, "$V%02d,%02d\r\n", crate, module);
  writeToCrate(crate,sbuff);
}

void request_vthb(int crate, int module)
{
  char sbuff[256];
  
  /* Request threshold B voltage */
  sprintf (sbuff, "$W%02d,%02d\r\n", crate, module);
  writeToCrate(crate, sbuff);
}

void request_VTp(int crate,int module)
{
  char sbuff[256];
  sprintf (sbuff, "$X%02d,%02d\r\n", crate, module);
  writeToCrate(crate,sbuff);
}

void request_Temp(int crate,int module)
{
  char sbuff[256];
  sprintf (sbuff, "$T%02d,%02d\r\n", crate, module);
  writeToCrate(crate,sbuff);
}

void request_Volt(int crate,int module)
{
  char sbuff[256];
  sprintf (sbuff, "$P%02d,%02d\r\n", crate, module);
  writeToCrate(crate,sbuff);
}

void request_TPEnable(int crate)
{
  char sbuff[256];
  sprintf (sbuff, "$F%02d,00\r\n", crate);
  writeToCrate(crate,sbuff);
}

void request_MaxTemp(int crate)
{
  char sbuff[256];
  sprintf (sbuff, "$T%02d,00\r\n", crate);
  writeToCrate(crate,sbuff);
}

void set_pavtp(int crate,int module,float newvtp)
{
  char buff[256];
  int status = 0;

  /* Set new VTP information for a PA channel */

  sprintf (buff, "$U%02d,%02d,-%04d\r\n", crate, module, (int)fabs(newvtp));

  printf("set_pavtp: crate %d, module %d, value %f, write %s\n",crate,module,newvtp,buff);

  status = serial_write(fd[findchain(crate)], buff);
  assert(status == 0);
}

void set_pavthresh(int crate,int module,float newvalue)
{
  char buff[256];
  float thresh;
  int status;
  int chan = get_pa_chan(crate,module);

  if (chan < 0)
    {
      cm_msg(MERROR, "set_pavthresh", "Invalid crate %d and module %d", crate, module);
      return;
    }

  /* Set new Thresh information for a PA channel */

  thresh = 0.5 + (1. + ThreshCaliA[chan]) * fabs(newvalue) + ThreshCaliB[chan];

  sprintf (buff, "$S%02d,%02d,-%04d\r\n", crate, module, (int)thresh);

  printf("set_pavthresh: crate %d, module %d, value %f %f, write %s\n",crate,module,newvalue,thresh,buff);

  status = serial_write(fd[findchain(crate)], buff);
  assert(status == 0);
}

void set_patpe(int crate)
{
  int status;
  char buff[256];
  
  /* Set TPE (Test Pulse Enable) for this crate */
  
  if (D_TPEnable [crate])
    sprintf (buff, "$E%02d,00\r\n", crate);
  else
    sprintf (buff, "$D%02d,00\r\n", crate);

  printf("set_patpe: crate %d, TPEnable %d, write %s\n",crate,(int)D_TPEnable[crate],buff);
  
  status = serial_write(fd[findchain(crate)], buff);
  assert(status==0);
}

INT get_pa_address(int chan, int *crate, int *module)
{
  (*crate)  = chan/SLOTSPERCRATE;
  (*module) = 1+(chan%SLOTSPERCRATE);

  return 0;
}

void sendRequestsCrate(int crate,int req)
{
  //printf("sendRequestsCrate crate %d, req %d, doWriteCrate %d\n", crate, req, doWriteCrate[crate]);

  if (doWriteCrate[crate])
    {
      if (doWriteCrate[crate] & WRITE_TPE) set_patpe(crate);
      doWriteCrate[crate] = 0;
    }
  
  switch (req&1)
    {
    case 0:
      request_TPEnable(crate);
      break;
    case 1:
      request_MaxTemp(crate);
      break;
    }
}

void sendRequestsModule(int chan,int crate,int module,int req)
{
  if (doWriteChannel[chan])
    {
      if (doWriteChannel[chan] & WRITE_VTP) set_pavtp(crate,module,D_VTp[chan]);
      if (doWriteChannel[chan] & WRITE_VTH) set_pavthresh(crate,module,D_Thresh[chan]);
      doWriteChannel[chan] = 0;
    }

  switch (req)
    {
    case READ_VTHA:
      request_vtha(crate,module);
      break;
    case READ_VTHB:
      request_vthb(crate,module);
      break;
    case READ_VTP:
      request_VTp(crate,module);
      break;
    case READ_TEMP:
      request_Temp(crate,module);
      break;
    case READ_VOLT:
      request_Volt(crate,module);
      break;
    }
}

void  checkChannelTimeout(ScData_t* scData,
			 const char* name,
			 int chan,
			 time_t now,
			 time_t lastTime[],
			 int req)
{
  int t;
  int crate, module;

  if (lastTime[chan] == 0)
    {
      lastTime[chan] = now;
      return;
    }

  get_pa_address(chan,&crate,&module);
  
  t = now - lastTime[chan];
  
  if (t > 3*gCycleTime)
    {
      if (channelTimeoutReq[chan] == 0)
	{
	  channelTimeoutReq[chan]  = req;
	  channelTimeoutTime[chan] = now;
	}

      if (t > 8*gCycleTime)
	{
	  SetScStatus(scData,chan,ST_UNKNOWN,"PostAmp channel %d, crate %d, module %d: timeout waiting for %s reply: %d seconds, times: %d %d",chan,crate,module,name,now-lastTime[chan],now,lastTime[chan]);
	}
    }
}
  
void checkCrateTimeout(ScData_t* scData,
		       const char* name,
		       int crate,
		       time_t now,
		       time_t lastTime[])
{
  if (lastTime[crate] == 0)
    lastTime[crate] = now;
  else
    {
      if (now - lastTime[crate] > 2*gCycleTime)
	{
	  //sendRequestsCrate(crate);
	}

      if (now - lastTime[crate] > 5*gCycleTime)
	{
	  SetScStatus(scData,crate,ST_UNKNOWN,"PostAmp crate %d: timeout waiting for %s reply: %d sec, times: %d %d",crate,name,now-lastTime[crate],now,lastTime[crate]);
	}
    }
}
  
int set_if_worse (int status, int newstatus)
{
  /* Return the worse of the two status values passed to me
   * (except if the current status is DISABLED or NOT_IMPLEMENTED)
   */
  if (status == ST_DISABLE || status == ST_NOTIMP || status > newstatus)
    return (status);
  else
    return (newstatus);
}

void checkChannelStatus(int chan)
{
  /* Check status of all PA variables and update status for a channel */

  if (PA_missing (chan))
    {
      SetScStatus(scPAStatus,chan,ST_NOTIMP,"Not implemented");
      SetScStatus(vtpData,   chan,ST_NOTIMP,"Not implemented");
      SetScStatus(thraData,  chan,ST_NOTIMP,"Not implemented");
      SetScStatus(thrbData,  chan,ST_NOTIMP,"Not implemented");
      SetScStatus(tempData,  chan,ST_NOTIMP,"Not implemented");
      SetScStatus(vplusData, chan,ST_NOTIMP,"Not implemented");
      SetScStatus(vminusData,chan,ST_NOTIMP,"Not implemented");
    }
  else
    {
      DWORD newstatus = ST_OK;
      newstatus = set_if_worse (newstatus, vtpData->fStatus[chan]);
      newstatus = set_if_worse (newstatus, thraData->fStatus[chan]);
      newstatus = set_if_worse (newstatus, thrbData->fStatus[chan]);
      newstatus = set_if_worse (newstatus, tempData->fStatus[chan]);
      newstatus = set_if_worse (newstatus, vplusData->fStatus[chan]);
      newstatus = set_if_worse (newstatus, vminusData->fStatus[chan]);
      SetScStatus(scPAStatus,chan,newstatus,"new status");
    }
}

void update_CrateStatus(int crate)
{
  if (PACtrl_missing (crate))
    {
      SetScStatus(scPACtrlStatus,crate,ST_NOTIMP,"not implemented");
    }
  else
    {
      int newstatus = ST_OK;
      newstatus = set_if_worse (newstatus, scTPEnableStatus->fStatus[crate]);
      newstatus = set_if_worse (newstatus, maxtempData->fStatus[crate]);
      SetScStatus(scPACtrlStatus,crate,newstatus,"new status");
    }

  WriteScStatus(hDB,scPACtrlStatus);
}

void dispatch(int chain,const char*buff)
{
  const char *s;
  time_t now = time(NULL);

  for (s=buff; *s; s++)
    if (*s == '#')
      buff = s;

  if (strncmp(buff,"#V",2) == 0)
    {
      int crate;
      int module;
      int value;
      int chan;

      int status = sscanf (buff, "#V%2d,%2d,%5d", &crate, &module, &value);
      if (status != 3)
	{
	  printf("dispatch: chain %d: Bad PostAmp data: \"%s\"\n", chain, buff);
	  return;
	}

      chan = get_pa_chan(crate,module);
      if (chan < 0)
	{
	  printf("dispatch: chain %d: Invalid crate %d and module %d in %s\n", chain, crate, module, buff);
	  return;
	}

      /* For PACTs (serno>=3500) this is the 24V line (and this is all there is) */
      if (PASerialNo[chan] >= 3500)
	{
	  SetScValue(thraData,chan,value,value/100.0);
	  
	  if (fabs(GetScValue(thraData,chan) - 24.0) > 2.0)
	    SetScStatus(thraData,chan,ST_PAV24,"Bad PACT 24V",GetScValue(thraData,chan));
	  else
	    SetScStatus(thraData,chan,ST_OK,"no error");
	}
      else
	{
	  static int thraBadCount[NUMPACHANS];

	  SetScValue(thraData,chan,value,value);

	  if (fabs(fabs(GetScValue(thraData,chan)) - fabs(D_Thresh[chan])) > VThreshSensitivity)
	    {
	      thraBadCount[chan] ++;

	      printf("Bad PostAmp %d Threshold A %f should be %f, count %d\n",
		     chan,
		     GetScValue(thraData,chan),
		     D_Thresh[chan],
		     thraBadCount[chan]);

	      if (thraBadCount[chan] > 1)
		SetScStatus(thraData,chan,ST_PAVTHRESHA,"Bad PostAmp Threshold A should be %f",D_Thresh[chan]);
	    }
	  else
	    {
	      thraBadCount[chan] = 0;
	      SetScStatus(thraData,chan,ST_OK,"no error");
	    }
	}

      thraTime[chan] = now;

      checkChannelStatus(chan);
    }
  else if (strncmp(buff,"#W",2) == 0)
    {
      int crate;
      int module;
      int value;
      int chan;

      int status = sscanf (buff, "#W%2d,%2d,%5d", &crate, &module, &value);
      if (status != 3)
	{
	  printf("dispatch: chain %d: Bad PostAmp data: \"%s\"\n", chain, buff);
	  return;
	}

      chan = get_pa_chan(crate,module);
      if (chan < 0)
	{
	  printf("dispatch: chain %d: Invalid crate %d and module %d in \"%s\"\n", chain, crate, module, buff);
	  return;
	}


      /* For PACTs (serno>=3500) this is what??? */
      if (PASerialNo[chan] >= 3500)
	{
	  SetScValue(thrbData,chan,value,value/10.0);
	  SetScStatus(thrbData,chan,ST_OK,"no error");
	}
      else
	{
	  static int thrbBadCount[NUMPACHANS];

	  SetScValue(thrbData,chan,value,value);

	  if (fabs(fabs(GetScValue(thrbData,chan)) - fabs(D_Thresh[chan])) > VThreshSensitivity)
	    {
	      thrbBadCount[chan] ++;

	      printf("Bad PostAmp %d Threshold B %f should be %f, count %d\n",
		     chan,
		     GetScValue(thrbData,chan),
		     D_Thresh[chan],
		     thrbBadCount[chan]);

	      if (thrbBadCount[chan] > 1)
		SetScStatus(thrbData,chan,ST_PAVTHRESHB,"Bad PostAmp Threshold B should be %f",D_Thresh[chan]);
	    }
	  else
	    {
	      thrbBadCount[chan] = 0;
	      SetScStatus(thrbData,chan,ST_OK,"no error");
	    }
	}

      thrbTime[chan] = now;

      checkChannelStatus(chan);
    }
  else if (strncmp(buff,"#X",2) == 0)
    {
      int crate;
      int module;
      int value;
      int chan;

      int status = sscanf (buff, "#X%2d,%2d,%5d", &crate, &module, &value);
      if (status != 3)
	{
	  printf("dispatch: chain %d: Bad PostAmp data: \"%s\"\n", chain, buff);
	  return;
	}

      chan = get_pa_chan(crate,module);
      if (chan < 0)
	{
	  printf("dispatch: chain %d: Invalid crate %d and module %d in \"%s\"\n", chain, crate, module, buff);
	  return;
	}

      SetScValue(vtpData,chan,value,value);

      if (fabs(fabs(value) - fabs(D_VTp[chan])) > VTPSensitivity)
	SetScStatus(vtpData,chan,ST_PAVTP,"Bad PostAmp VTp should be %f",D_VTp[chan]);
      else
	SetScStatus(vtpData,chan,ST_OK,"no error");

      vtpTime[chan] = now;

      checkChannelStatus(chan);
    }
  else if (strncmp(buff,"#T",2) == 0)
    {
      int crate;
      int module;
      int value;

      int status = sscanf (buff, "#T%2d,%2d,%5d", &crate, &module, &value);
      if (status != 3)
	{
	  printf("dispatch: chain %d: Bad PostAmp data: \"%s\"\n", chain, buff);
	  return;
	}

      if (module == 0)
	{
	  if (crate<0 || crate>=NUMPACRATES)
	    {
	      printf("dispatch: chain %d: Invalid crate %d in \"%s\"\n", chain, crate, buff);
	      return;
	    }
	  
	  SetScValue(maxtempData,crate,value,value/10.0);
	  
	  if (crate == 11)
	    {
	      if (GetScValue(maxtempData,crate) > TempAlarmPACT)
		SetScStatus(maxtempData,crate,ST_PATEMP,"PACT crate too hot %f",GetScValue(maxtempData,crate));
	      else
		SetScStatus(maxtempData,crate,ST_OK,"no error");
	    }
	  else
	    {
	      if (GetScValue(maxtempData,crate) > TempAlarm)
		SetScStatus(maxtempData,crate,ST_PATEMP,"PostAmp crate too hot %f",GetScValue(maxtempData,crate));
	      else
		SetScStatus(maxtempData,crate,ST_OK,"no error");
	    }
	  
	  WriteScValues(hDB,maxtempData);
	  WriteScStatus(hDB,maxtempData);

	  maxtempTime[crate] = now;

	  update_CrateStatus(crate);
	}
      else
	{
	  int chan;
	  float maxvalue;

	  chan = get_pa_chan(crate,module);
	  if (chan < 0)
	    {
	      printf("dispatch: chain %d: Invalid crate %d and module %d in \"%s\"\n", chain, crate, module, buff);
	      return;
	    }
	  
	  SetScValue(tempData,chan,value,value/10.0);
	  
	  if (PASerialNo[chan] >= 3500)
	    maxvalue = TempAlarmPACT;
	  else
	    maxvalue = TempAlarm;
	  
	  if (GetScValue(tempData,chan) > maxvalue)
	    SetScStatus(tempData,chan,ST_PATEMP,"PostAmp too hot");
	  else
	    SetScStatus(tempData,chan,ST_OK,"no error");

	  tempTime[chan] = now;

	  checkChannelStatus(chan);
	}
    }
  else if (strncmp(buff,"#P",2) == 0)
    {
      int crate;
      int module;
      int value1, value2;
      int chan;

      int status = sscanf (buff, "#P%2d,%2d,%5d,%5d", &crate, &module, &value1, &value2);
      if (status != 4)
	{
	  printf("dispatch: chain %d: Bad PostAmp data: \"%s\"\n", chain, buff);
	  return;
	}

      chan = get_pa_chan(crate,module);
      if (chan < 0)
	{
	  printf("dispatch: chain %d: Invalid crate %d and module %d in \"%s\"\n", chain, crate, module, buff);
	  return;
	}

      SetScValue(vplusData,chan,value1,value1/1000.0);
      SetScValue(vminusData,chan,value2,value2/1000.0);

      if (fabs(GetScValue(vplusData,chan) - 5.0) > VoltSensitivity)
	SetScStatus(vplusData,chan,ST_PAVPLUS,"Bad PostAmp +5V");
      else
	SetScStatus(vplusData,chan,ST_OK,"no error");

      if (fabs(GetScValue(vminusData,chan) + 5.0) > VoltSensitivity)
	SetScStatus(vminusData,chan,ST_PAVMINUS,"Bad PostAmp -5V");
      else
	SetScStatus(vminusData,chan,ST_OK,"no error");

      voltageTime[chan] = now;

      checkChannelStatus(chan);
    }
  else if (strncmp(buff,"#F",2) == 0)
    {
      int crate;
      int value;
      int size;

      int status = sscanf (buff, "#F%2d,00,%d", &crate, &value);
      if (status != 2)
	{
	  printf("dispatch: chain %d: Bad PostAmp data: \"%s\"\n", chain, buff);
	  return;
	}
      
      if (crate<0 || crate>=NUMPACRATES)
	{
	  printf("dispatch: chain %d: Invalid crate %d in \"%s\"\n", chain, crate, buff);
	  return;
	}

      M_TPEnable[crate] = value;
      
      if (M_TPEnable[crate] != D_TPEnable[crate])
	{
	  if (M_TPEnable[crate])
	    SetScStatus(scTPEnableStatus,crate,ST_PATPENABLE,"PostAmp TP is enabled");
	  else
	    SetScStatus(scTPEnableStatus,crate,ST_PATPDISABLE,"PostAmp TP is disabled");
	}
#if 0
      else if (M_TPEnable[crate] == 0)
	SetScStatus(scTPEnableStatus,crate,ST_PATPISDISABLED,"PostAmp TP is disabled");
#else
      else if (M_TPEnable[crate] != 0)
	SetScStatus(scTPEnableStatus,crate,ST_PATPISENABLED,"PostAmp TP is enabled");
#endif
      else
	SetScStatus(scTPEnableStatus,crate,ST_OK,"no error");
      
      size = NUMPACRATES * sizeof (BOOL);
      status = db_set_data (hDB, hMTPEnable, M_TPEnable, size, NUMPACRATES, TID_BOOL);
      if (status != SUCCESS)
	{
	  cm_msg (MERROR, frontend_name, "Error setting  M_TPEnable, db_set_data() status %d", status);
	}

      tpeTime[crate] = now;

      update_CrateStatus(crate);
    }
  else
    {
      printf("dispatch: chain %d: Unexpected PostAmp data: \"%s\"\n", chain, buff);
    }
}

INT xread_PA_Event(char *pevent, int ott);

INT read_PA_Event(char *pevent, int ott)
{
  //DWORD t2,t1 = ss_millitime();
  //printf("read_pa_event %p %d, start\n",pevent,ott);
  INT ret = xread_PA_Event(pevent,ott);
  //printf("read_pa_event %p %d, end\n",pevent,ott);
  //t2 = ss_millitime();
  //printf("%d...",t2-t1); fflush(stdout);
  return ret;
}

void sendRequests()
{
  static int crate=0, module=0, req=1;
  int chan;
  int xcrate;
  static int xchan = 0;
  static int dotimeouts = 0;

  static int cratemap[12] = { 0, 1, 6, 7, 2, 3, 8, 9, 4, 5, 10, 11 };

#define BYCHAN 1

#if BYCHAN
  get_pa_address(xchan,&crate,&module);
#endif

  //printf("xchan %d, crate %d, module %d\n", xchan, crate, module);

  //xcrate = cratemap[crate];
  xcrate = crate;

  chan = get_pa_chan(xcrate,module);

  //printf("xchan %d, chan %d, crate %d, module %d, req %d\n",xchan,chan,xcrate,module,req);

#if BYCHAN
  if (module == 1)
    {
      sendRequestsCrate(xcrate,req);
    }
#endif

  if (module == 0)
    {
      sendRequestsCrate(xcrate,req);
    }
  else if (!PA_missing(chan))
    {
      if (dotimeouts)
	{
	  if (channelTimeoutReq[chan] > 0)
	    {
	      printf("chan %d, crate %d, module %d, timeout req %d\n",chan,xcrate,module,channelTimeoutReq[chan]);
	      sendRequestsModule(chan,xcrate,module,channelTimeoutReq[chan]);
	      channelTimeoutReq[chan] = 0;
	    }
	  req = MAX_REQ+1; // force skip to next channel
	}
      else
	{
	  sendRequestsModule(chan,xcrate,module,req);
	}
    }

  req++;
  if (req<=MAX_REQ)
    return;
  req = 1;

  xchan++;
  if (xchan<NUMPACHANS)
    return;
  xchan = 0;

  if (1)
    {
      static time_t prev = 0;
      time_t now;
      int cycletime;
      time(&now);

      if (prev!=0)
	{
	  cycletime = now-prev;
	  if (1 || cycletime < gCycleTime)
	    {
	      //printf("cycle time: %d sec\n",cycletime);
	      if (!dotimeouts)
		gCycleTime = cycletime;
	    }
	}

      dotimeouts = !dotimeouts;

      //printf("timeout mode %d\n",dotimeouts);

      prev = now;
    }
}

int readResponses()
{
  int i, count;

  count = 0;

  for (i=0; i<gNumChains; i++)
    {
      while (1)
	{
	  char buf[256];

	  int status = serial_readline(fd[i], buf, sizeof(buf), -1000);
	  if (status == 0)
	    {
	      //printf("timeout fd[%d], in flight: %d\n", i, inflight[i]);
	      //sleep(1);
	      break;
	    }

	  if (status == -1) // EOF/disconnected or I/O error
	    {
	      cm_msg (MERROR, "read_pa_event", "Error reading data from chain %d, status %d", i, status);
	      set_equipment_status(EQ_NAME, "Communication error", "red");
	      exit(1);
	    }

	  if (status == -2) // timeout while reading a line of data
	    {
	      //printf("timeout:  chain %d: got [%s]\n",i,buf);
	      //sleep(1);
	      break;
	    }

	  if (status <= 0)
	    {
	      cm_msg (MERROR, "read_pa_event", "Error reading data from chain %d, in flight data: %d, status %d", i, inflight[i], status);
	      inflight[i] = 0;
	      break;
	    }

	  //printf("read chain %d, fd %d: %s\n", i, fd[i], buf);

	  inflight[i]--;

	  dispatch(i,buf);
	  count++;
	}
    }

  return count;
}

INT xread_PA_Event(char *pevent, int ott)
{
  time_t now;
  static time_t lastFlush = 0;
  int i, count;

  count = readResponses();

  //printf("received: %d, in flight: %3d %3d\n",count,inflight[0],inflight[1]);

  sendRequests();

  now = time(NULL);

  for (i=0; i<NUMPACHANS; i++)
    if (scPAStatus->fStatus[i] != ST_NOTIMP)
      {
	checkChannelTimeout(thraData,"ThresholdA",i,now,thraTime,READ_VTHA);
	checkChannelTimeout(thrbData,"ThresholdB",i,now,thrbTime,READ_VTHB);
	checkChannelTimeout(vtpData,"VTp",i,now,vtpTime,READ_VTP);
	checkChannelTimeout(tempData,"Temperature",i,now,tempTime,READ_TEMP);
	checkChannelTimeout(vplusData,"VoltageP",i,now,voltageTime,READ_VOLT);
	checkChannelTimeout(vminusData,"VoltageM",i,now,voltageTime,READ_VOLT);
	checkChannelStatus(i);
      }
  
  for (i=0; i<NUMPACRATES; i++)
    {
      checkCrateTimeout(maxtempData,"MaxTemp",i,now,maxtempTime);
      checkCrateTimeout(scTPEnableStatus,"TPEnable",i,now,tpeTime);
    }
  
  if (now - lastFlush > 15)
    {
      lastFlush = now;

      WriteScValues(hDB,thraData);
      WriteScStatus(hDB,thraData);

      WriteScValues(hDB,thrbData);
      WriteScStatus(hDB,thrbData);

      WriteScValues(hDB,vtpData);
      WriteScStatus(hDB,vtpData);

      WriteScValues(hDB,tempData);
      WriteScStatus(hDB,tempData);

      WriteScValues(hDB,vplusData);
      WriteScStatus(hDB,vplusData);

      WriteScValues(hDB,vminusData);
      WriteScStatus(hDB,vminusData);

      WriteScStatus(hDB,scPAStatus);

      WriteScValues(hDB,maxtempData);
      WriteScStatus(hDB,maxtempData);
      WriteScStatus(hDB,scPACtrlStatus);
    }

  //ss_sleep (10);
  return 0;
}

/* load PostAmp serial numbers, used to tell PACTs and empty slots */

int load_PA_SerialNo (INT *PASerialNo, INT *PACtrlSerialNo)
{
  /* Eventually from the database, but not there yet.
   */
  int status;

  status = my_find_and_load(hDB, hSettingRoot, "PASerialNo", NULL, PASerialNo, NUMPACHANS, TID_INT);
  if (status != SUCCESS) {
    return (status);
  }
  status = my_find_and_load(hDB, hSettingRoot, "PACtrlSerialNo", NULL, PACtrlSerialNo, NUMPACRATES, TID_INT);
  if (status != SUCCESS) {
    return (status);
  }
  return (status);
}

/* load PostAmp calibrations */

#if 1
#include "e614db.h"

int load_PA_Cali (float *ThreshCaliA, float *ThreshCaliB)
{
  int i;
  int status;
  double a, b;
  HNDLE hKey;
  int size;
  PGconn *conn;

  printf("Loading PostAmp threshold calibrations!\n");

  //conn = e614db_default_connect();
  if (1 || !conn || (PQstatus(conn) == CONNECTION_BAD) ) {
    char text[256];
    //sprintf(text,"%s cannot connect to calibrations database, see messages",frontend_name);
    //al_trigger_alarm(frontend_name,text,"Warning","",AT_INTERNAL);
    //cm_msg (MERROR, "load_PA_Cali","Cannot connect to calibration database, error: %s",PQerrorMessage(conn));

    /* if cannot connect to database, read previous values from ODB */
    cm_msg (MINFO, "load_PA_Cali","Reading PA calibrations from ODB");

    status = my_find_and_load(hDB, hSettingRoot, "ThreshCaliA", NULL, ThreshCaliA, NUMPACHANS, TID_FLOAT);
    if (status != SUCCESS) return (status);

    status = my_find_and_load(hDB, hSettingRoot, "ThreshCaliB", NULL, ThreshCaliB, NUMPACHANS, TID_FLOAT);
    if (status != SUCCESS) return (status);

    return (SUCCESS);

  } else {

    int nocalibration = 0;

    /* read calibrations from database */
  
    for (i=0; i<NumChan; i++) {
      /* No calibration for the PACT units (or anything missing) */
      if (PASerialNo[i] >= 3500 || PA_missing(i)) {
	a=0.0;
	b=0.0;
	status = 0;
      } else {
	status = e614db_get_PostAmp_calib(conn, PASerialNo[i], time(NULL), /* get current */ &a, &b);
        printf("PostAmp %d, PASerialNo %d, values %f and %f, status %d\n", i, PASerialNo[i], a, b, status);
	if(status == 0) {
	  ThreshCaliA [i] = a;
	  ThreshCaliB [i] = b;
	} else {
	  ThreshCaliA [i] = 0;
	  ThreshCaliB [i] = 0;
	  cm_msg (MERROR, "load_PA_Cali", "No calibrations for PA SerialNo %d",PASerialNo[i]);
	  nocalibration++;
	}
      }
    }

    e614db_close(conn);

    if (nocalibration > 0) {
      cm_msg (MERROR, "load_PA_Cali", "No calibrations for %d PAs",nocalibration);
      return (FE_ERR_ODB);
    }

    /* Store database values in the odb */

    status = db_find_key(hDB, hSettingRoot, "ThreshCaliA", &hKey);
    if (status != SUCCESS) {
      printf("A status %d\n", status);
      return (status);
    }
    size = NUMPACHANS * sizeof (float);
    status = db_set_data (hDB, hKey, ThreshCaliA, size, NUMPACHANS, TID_FLOAT);
    if (status != SUCCESS) {
      printf("B status %d\n", status);
      return (status);
    }
    status = db_find_key(hDB, hSettingRoot, "ThreshCaliB", &hKey);
    if (status != SUCCESS) {
      printf("C status %d\n", status);
      return (status);
    }
    size = NUMPACHANS * sizeof (float);
    status = db_set_data (hDB, hKey, ThreshCaliB, size, NUMPACHANS, TID_FLOAT);
    if (status != SUCCESS) {
      printf("D status %d\n", status);
      return (status);
    }
  }
  
  return (SUCCESS);
}
#endif

/* load all settings from ODB */

int get_settings()
{
  int status;

  status = my_find_and_load(hDB, hSettingRoot, "VTPSensitivity", NULL, &VTPSensitivity, 1, TID_FLOAT);
  if (status != SUCCESS) {
    return (status);
  }

  status = my_find_and_load(hDB, hSettingRoot, "VThreshSensitivity", NULL, &VThreshSensitivity, 1, TID_FLOAT);
  if (status != SUCCESS) {
    return (status);
  }

  status = my_find_and_load(hDB, hSettingRoot, "TempSensitivity", NULL, &TempSensitivity, 1, TID_FLOAT);
  if (status != SUCCESS) {
    return (status);
  }

  status = my_find_and_load(hDB, hSettingRoot, "TempAlarm", NULL, &TempAlarm, 1, TID_FLOAT);
  if (status != SUCCESS) {
    return (status);
  }

  status = my_find_and_load(hDB, hSettingRoot, "TempAlarmPACT", NULL, &TempAlarmPACT, 1, TID_FLOAT);
  if (status != SUCCESS) {
    return (status);
  }

  status = my_find_and_load(hDB, hSettingRoot, "VoltSensitivity", NULL, &VoltSensitivity, 1, TID_FLOAT);
  if (status != SUCCESS) {
    return (status);
  }

  status = my_find_and_load(hDB, hVarRoot, "D_VTp", &hDVTp, D_VTp, NUMPACHANS, TID_FLOAT);
  if (status != SUCCESS) {
    return (status);
  }
  
  status = my_find_and_load(hDB, hVarRoot, "D_Thresh", &hDThresh, D_Thresh, NUMPACHANS, TID_FLOAT);
  if (status != SUCCESS) {
    return (status);
  }
  
  status = my_find_and_load(hDB, hVarRoot, "D_TP", &hDTPEnable, D_TPEnable, NUMPACRATES, TID_BOOL);
  if (status != SUCCESS) {
    return (status);
  }

  return (SUCCESS);
}

int config(void)
{
  int i;
  INT status;

  /* Get PA Update Period
   */
  status = my_find_and_load(hDB, hSettingRoot, "Period", NULL, &Period,
			     1, TID_INT);
  if (status != SUCCESS) {
    return (status);
  }
  
  /* Number of channels
   */
  status = my_find_and_load(hDB, hSettingRoot, "NumChan", NULL, &NumChan,
			     1, TID_INT);
  if (status != SUCCESS) {
    return (status);
  }

  NumCrate = (NumChan + SLOTSPERCRATE - 1) / SLOTSPERCRATE;

  printf("config: %d channels, %d crates\n", NumChan, NumCrate);

  for (i=0; i<NUMPACHANS; i++)
    PASerialNo [i] = -1;

  for (i=0; i<NUMPACRATES; i++)
    PACtrlSerialNo [i] = -1;

  /* PostAmp Serial No
   * We do this first, because illegal Serial Number indicates a missing module
   */

  status = load_PA_SerialNo (PASerialNo, PACtrlSerialNo);
  if (status != SUCCESS) {
    return (status);
  }

  for (i=0; i<NUMPACHANS; i++)
   ThreshCaliA  [i] = -99.9;

  for (i=0; i<NUMPACHANS; i++)
   ThreshCaliB  [i] = -99.9;

  vtpData  = NewScData(hDB,hVarRoot,  "/status/postamp/vtp",      "M_VTp",NUMPACHANS,0,0);
  thraData = NewScData(hDB,hVarRoot,  "/status/postamp/M_ThreshA","M_ThreshA",NUMPACHANS,0,0);
  thrbData = NewScData(hDB,hVarRoot,  "/status/postamp/M_ThreshB","M_ThreshB",NUMPACHANS,0,0);
  tempData = NewScData(hDB,hVarRoot,  "/status/postamp/temp",  "Temp",NUMPACHANS,0,0);
  vplusData = NewScData(hDB,hVarRoot, "/status/postamp/vplus", "VoltageP",NUMPACHANS,0,0);
  vminusData = NewScData(hDB,hVarRoot,"/status/postamp/vminus","VoltageM",NUMPACHANS,0,0);

  scPAStatus = NewScData(hDB,0,"/status/postamp/chan","PAStatus",NUMPACHANS,0,0);

  for (i=0; i< NUMPACHANS; i++)
    {
      if (PA_missing(i) || i>= NumChan)
	{
	  SetScStatus(scPAStatus,i,ST_NOTIMP,"Not implemented");
	  SetScStatus(vtpData ,i,ST_NOTIMP,"Not implemented");
	  SetScStatus(thraData,i,ST_NOTIMP,"Not implemented");
	  SetScStatus(thrbData,i,ST_NOTIMP,"Not implemented");
	  SetScStatus(tempData,i,ST_NOTIMP,"Not implemented");
	  SetScStatus(vplusData,i,ST_NOTIMP,"Not implemented");
	  SetScStatus(vminusData,i,ST_NOTIMP,"Not implemented");
	}
    }

  WriteScStatus(hDB,scPAStatus);

  maxtempData = NewScData(hDB,hVarRoot,"/status/postamp/maxtemp","MaxTemp",NUMPACRATES,0,0);
  scTPEnableStatus = NewScData(hDB,0,0,"TPEnableStatus",NUMPACRATES,0,0);
  scPACtrlStatus   = NewScData(hDB,0,"/status/postamp/ctrl","PACtrlStatus",NUMPACRATES,0,0);

  for (i=0; i< NUMPACRATES; i++)
    {
      if (PACtrl_missing(i) || i>= NumCrate)
	{
	  SetScStatus(scPACtrlStatus,i,ST_NOTIMP,"Not implemented");
	  SetScStatus(scTPEnableStatus,i,ST_NOTIMP,"Not implemented");
	  SetScStatus(maxtempData,i,ST_NOTIMP,"Not implemented");
	}
      else
	{
	  SetScStatus(scPACtrlStatus,i,ST_UNKNOWN,"Status unknown");
	  SetScStatus(scTPEnableStatus,i,ST_UNKNOWN,"Status unknown");
	}
    }

  WriteScStatus(hDB,scPACtrlStatus);

  /* Threshold Calibration
   */
#if 1
  status = load_PA_Cali (ThreshCaliA, ThreshCaliB);
  if (status != SUCCESS) {
    return (status);
  }
#endif

  printf("HereA!\n");

  /* Get handles for various things which we monitor
   */
  status = db_find_key(hDB, hVarRoot, "M_TP", &hMTPEnable); // TID_BOOL
  if (status != SUCCESS) {
    return (status);
  }
  
  printf("HereB!\n");

  status = get_settings();

  printf("HereC!\n");

  return (status);
}


int write_D_TP()
{
  int size = NUMPACRATES * sizeof (BOOL);
  int status = db_set_data (hDB, hDTPEnable, D_TPEnable, size, NUMPACRATES, TID_BOOL);
  if (status != SUCCESS)
    {
      cm_msg (MERROR, frontend_name, "Error setting  D_TPEnable, db_set_data() status %d", status);
    }
  return status;
}
  
int write_D_Thresh()
{
  int size = NUMPACHANS * sizeof (float);
  int status = db_set_data (hDB, hDThresh, D_Thresh, size, NUMPACHANS, TID_FLOAT);
  if (status != SUCCESS)
    {
      cm_msg (MERROR, frontend_name, "Error setting  D_Thresh, db_set_data() status %d", status);
    }
  return status;
}
  
int write_D_VTp()
{
  int size = NUMPACHANS * sizeof (float);
  int status = db_set_data (hDB, hDVTp, D_VTp, size, NUMPACHANS, TID_FLOAT);
  if (status != SUCCESS)
    {
      cm_msg (MERROR, frontend_name, "Error setting  D_VTp, db_set_data() status %d", status);
    }
  return status;
}
  
/* Set new threshold on one or more channels */

void new_vth (char *s)
{
  int beg, end;
  int i;
  float vth;
  
  /* Flush past the command wordc
   */
  while (*s) {
    if (isspace (*s)) break;
    ++s;
  }

  /* Expecting a threshold and (optionally) a channel
   */
  if (*s) {
    i = sscanf (s, "%f %d", &vth, &beg);
    if (i == 0) return;
    if (i == 1) {
      /* No channel - so do them all
       */
      beg = 0;
      end = NumChan;
    } else {
      if (beg < 0 || beg >= NumChan) return;
      end = beg+1;
    }
    
    for (i=beg; i<end; i++) {
      if (PA_missing(i)) continue;
      D_Thresh[i] = vth;
      doWriteChannel[i] |= WRITE_VTH;
    }

    write_D_Thresh();

    if (beg == end-1) // only one channel
      {
	int crate, module;
	i = beg;
	get_pa_address(i,&crate,&module);
	sendRequestsModule(i,crate,module,READ_VTHA);
	sendRequestsModule(i,crate,module,READ_VTHB);
      }
  }
}

/* set thresholds for all channels */

void new_gvth(char *s)
{
  int chan;
  int vth;

  vth = strtol(s+4,&s,0);

  printf("Set threshold to %d for all modules\n",vth);

  for (chan=0; chan<=NUMPACHANS; chan++)
    if (!PA_missing(chan))
      {
	D_Thresh[chan] = vth;
	doWriteChannel[chan] |= WRITE_VTH;
      }

  write_D_Thresh();
}

/* write VTest for one channel */

void new_vtest (char *s)
{
  int beg, end;
  int i;
  float vtp;
  
  /* Flush past the command wordc
   */
  while (*s) {
    if (isspace (*s)) break;
    ++s;
  }

  if (*s) {
    i = sscanf (s, "%f %d", &vtp, &beg);
    if (i == 0) return;
    if (i == 1) {
      /* No channel - so do them all
       */
      beg = 0;
      end = NumChan;
    } else {
      if (beg < 0 || beg >= NumChan) return;
      end = beg+1;
    }
    
    for (i=beg; i<end; i++) {
      if (PA_missing(i)) continue;
      D_VTp[i] = vtp;
      doWriteChannel[i] |= WRITE_VTP;
    }

    write_D_VTp();

    if (beg == end-1) // only one channel
      {
	int crate, module;
	i = beg;
	get_pa_address(i,&crate,&module);
	sendRequestsModule(i,crate,module,READ_VTP);
      }
  }
}

/* set Vtest for all channels */

void new_gvtest(char *s)
{
  int chan;
  int vtp;

  vtp = strtol(s+6,&s,0);

  printf("Set VTp to %d for all modules\n",vtp);

  for (chan=0; chan<=NUMPACHANS; chan++)
    if (!PA_missing(chan))
      {
	D_VTp[chan] = vtp;
	doWriteChannel[chan] |= WRITE_VTP;
      }

  write_D_VTp();
}

/* set TPEnable for one crate */

void new_tp(char *s)
{
  int crate;
  int onoff;

  crate = strtol(s+3,&s,0);
  onoff = strtol(s,&s,0);

  printf("Set crate %d TP to %d\n",crate,onoff);

  if (!PACtrl_missing(crate))
    {
      D_TPEnable[crate] = onoff;
      write_D_TP();
      doWriteCrate[crate] |= WRITE_TPE;
      sendRequestsCrate(crate,0);
      sendRequestsCrate(crate,1);
    }
}

/* set TPEnable for all crates */

void new_gtp(char *s)
{
  int crate;
  int onoff;

  onoff = strtol(s+4,&s,0);

  printf("Set TP to %d for all crates\n",onoff);

  for (crate=0; crate<=NUMPACRATES; crate++)
    if (!PACtrl_missing(crate))
      {
	D_TPEnable[crate] = onoff;
	doWriteCrate[crate] |= WRITE_TPE;
	sendRequestsCrate(crate,0);
	sendRequestsCrate(crate,1);
      }

  write_D_TP();
}

/* reload settings from ODB and write them into the PoastAmps */

void new_setall()
{
  int crate, chan;

  get_settings();

  for (crate=0; crate<=NUMPACRATES; crate++)
    if (!PACtrl_missing(crate))
      doWriteCrate[crate] |= WRITE_ALL;

  for (chan=0; chan<=NUMPACHANS; chan++)
    if (!PA_missing(chan))
      doWriteChannel[chan] |= WRITE_ALL;
}

void docmd (HNDLE hDB, HNDLE hKey, void *info)
{
  printf ("Got command %s via callback\n", cmdbuff);

  if (strncasecmp ("setall", cmdbuff, 6) == 0) {
    new_setall();
  } else if (strncasecmp ("vth", cmdbuff, 3) == 0) {
    new_vth(cmdbuff);
  } else if (strncasecmp ("gvth", cmdbuff, 4) == 0) {
    new_gvth(cmdbuff);
  } else if (strncasecmp ("vtest", cmdbuff, 5) == 0) {
    new_vtest(cmdbuff);
  } else if (strncasecmp ("gvtest", cmdbuff, 6) == 0) {
    new_gvtest(cmdbuff);
  } else if (strncasecmp ("tp", cmdbuff, 2) == 0) {
    new_tp(cmdbuff);
  } else if (strncasecmp ("gtp", cmdbuff, 3) == 0) {
    new_gtp(cmdbuff);
  }
}

// end file
