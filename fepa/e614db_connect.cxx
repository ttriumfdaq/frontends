/****************************************************************
 * $Id: e614db_connect.c,v 1.6 2005/08/04 00:29:34 twistonl Exp $
 * 
 * Production database name looks like "e614db15".
 * 15 is a version number. Largest existing number corresponds
 * to the current version.
 */

#include <stdio.h>
#include <string.h>
#include <libpq-fe.h>

#include "e614db.h"

/*================================================================*/
PGconn *e614db_connect(const char* host,
		       int port,
		       const char* username,
		       const char* password,
		       const char* dbname) 
{
  enum {STRBUFSIZE = 1024};
  char buf[STRBUFSIZE];
  char dbname_buf[STRBUFSIZE];
  PGconn *conn;
  PGresult *dbNameQ;

  /* A conservative estimate of the connection info text length */
  /* not including the string arguments is 100 */
  if(STRBUFSIZE < 100 + strlen(username) 
     + strlen(password) + strlen(dbname != NULL ? dbname: "")) 
    {
      return NULL;
    }

  /* Find out name of the current production database */
  if(dbname == NULL) {
    sprintf(buf, 
	    "host=%s port=%d user=%s password=%s dbname=template1", 
	    host, port, username, password
	    );
  
    conn = PQconnectdb(buf);
    
    /* check to see that the backend connection has been successfully made */
    if (PQstatus(conn) != CONNECTION_BAD) {
      /* Here we have a connection to an auxiliary database */
      dbNameQ = PQexec(conn, "SELECT pg_database.datname FROM pg_database;");

      if(PQresultStatus(dbNameQ) == PGRES_TUPLES_OK) {
	int i, maxNum = 0, curnum;
	for(i=0; i<PQntuples(dbNameQ); i++) {
	  if(1 == sscanf(PQgetvalue(dbNameQ, i, 0), 
			 "e614db%d", &curnum)) 
	    {
	      if(curnum > maxNum) maxNum = curnum;
	    }
	}
	
	if(maxNum) { /* Current DB version has been found */
	  sprintf(dbname_buf, "e614db%d", maxNum);
	  dbname = dbname_buf;
	}
      }
      PQclear(dbNameQ);
    }
    else { /* CONNECTION_BAD */
      return conn; /* Let the calling function deal this the problem */
    }
    /* Close the auxiliary connection */
    PQfinish(conn);
    conn = NULL;
  }

  /*----------------------------------------------------------------*
   * dbname has been either given by user or successfully found above.
   * Connect to the requested database 
   */
  if(dbname != NULL) {
    sprintf(buf, 
	    "host=%s port=%d user=%s password=%s dbname=%s",
	    host, port, username, password, dbname 
	    );
  
    conn = PQconnectdb(buf);
  }

  return conn;
}
/*================================================================*/
PGconn *e614db_default_connect() {
  return e614db_connect("tw04.triumf.ca", 5432, "dbreader", "123", NULL);
}
/*================================================================*/
void e614db_close(PGconn *conn) {
  PQfinish(conn);
}
/*============================ EOF ===============================*/
