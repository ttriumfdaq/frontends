/* Function prototypes */

#include "queue.h"

char *mymalloc (int nbytes);
void *myfree (void *);
INT my_find_and_load (HNDLE hDB,HNDLE hRoot, const char *name, HNDLE *hKey, void *data, int numvalues, int type);
struct quentry *qmalloc (void);
void qfree (struct quentry *pe);

// end file
