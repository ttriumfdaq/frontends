/* Various support routines for E614 Slow controls
 *
 * P. W. Green - April 2001
 */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include "midas.h"
#include "queue.h"

/* always use the fast queue element allocator */
#define FAKE_QMALLOC 1

/* malloc for which I don't have to check success
 * Note: calloc clears the memory to zero
 */
#ifdef FAKE_QMALLOC

#define QUEUESIZE 10000
static struct quentry qlist [QUEUESIZE];
static char qlstatus [QUEUESIZE];

int qecheck (int *s)
{
  int size = sizeof (struct quentry) / sizeof (int);
  int i;

  for (i=0; i<size; i++) {
    if (s[i] != 0) {
      return (1);
    }
  }
  return (0);
}

void qmcheck (void)
{
  int i, j;
  int *s;
  int size = sizeof (struct quentry) / sizeof (int);

  
  for (i=0;i<QUEUESIZE; i+=2) {
    s = (int *) &qlist [i];
    if (qecheck (s)) {
      printf("qmcheck: Overwrite error in malloc entry %d\n", i);
      for (j=0; j<size; j++)
	printf("%d %d\n", j, s[j]);
    }
  }
}

#endif

struct quentry *qmalloc (void)
{
  struct quentry *pe = NULL;

#ifdef FAKE_QMALLOC
  int i;

  for (i=1;i<QUEUESIZE; i+=2) {
    if (qlstatus [i] == 0) {
      qlstatus [i] = 1;
      pe = &qlist [i];
      //DB7PRINT ("Allocate block %d\n", i);
      break;
    }
  }
  if (pe == NULL) {
    printf("No more queue blocks left\n");
    exit (1);
  }
#else
  pe = (struct quentry *) mymalloc (sizeof (struct quentry));
#endif
  return (pe);
}

void qfree (struct quentry *pe)
{
#ifdef FAKE_QMALLOC

  int i;

  //qmcheck ();
  for (i=1;i<QUEUESIZE; i+=2) {
    if (pe == &qlist [i]) {
      qlstatus [i] = 0;
      memset (&qlist[i], sizeof (struct quentry), 0);
      //DB7PRINT ("Free block %d\n", i);
      break;
    }
  }
#else

  myfree (pe);

#endif
}

extern char* frontend_name;

char *mymalloc (int nbytes)
{
  char *s;

  s = (char*)calloc (nbytes, 1);
  if (s == NULL) {
    cm_msg(MERROR, frontend_name, "malloc failure - fatal");
    exit (1);
  }
  return (s);
}

void *myfree (void *s)
{
  if (s) free (s);
  return (NULL);
}

/* end file */
