#include <stdio.h>
#include "queue.h"

/* queue manipulation routines.
 *
 * Queue entries are assumed sorted by time (earliest first - i.e. at the
 * Head of the queue).
 *
 * Entries are removed from the head of the queue and (if time is
 * unimportant) inserted at the tail of the queue.
 */

struct quentry *popqueue (struct queue *pq)
{
  /* Remove entry at the head of the queue, and return pointer to it
   */

  struct quentry *pe;

  if (pq->num == 0)
    return (NULL);

  -- pq->num;
  pe = pq->head;
  pq->head = pe->next;

  return (pe);
}

void instail (struct quentry *pe, struct queue *pq)
{
  /* insert entry at the tail of the queue
   * (for those queues where time is unimportant)
   */
  struct quentry *peprev;

  peprev = pq->tail;
  if (peprev) {
    peprev->next = pe;
  }
  pe->next = NULL;
  pq->tail = pe;
  if (pq->num ++ == 0) {
    pq->head = pe;
  }
}

void insqueue (struct quentry *pe, struct queue *pq)
{
  /* Insert entry pe into queue pq based on time (earliest first)
   */

  struct quentry *peprev, *pethis;

  peprev = (struct quentry *)  &pq->head;
  while (1) {
    pethis = peprev->next;
    if (pethis == NULL) break;
    if (isbefore (pe->time, pethis->time)) break;
    peprev = pethis;
  }
  pe->next = pethis;
  peprev->next = pe;
  ++pq->num;
  if (pethis == NULL) {
    pq->tail = pe;
  } else if (pethis == (struct quentry *)&pq->head) {
    pq->head = pe;
  }
}

int isbefore (DWORD t1, DWORD t2)
{
  /* Check if time t1 comes before t2
   * Returns 1 if t1 is earlier, 0 if not
   *
   * The only "tricky" part here is that MIDAS uses an unsigned value
   * (DWORD) for times (ss_millitime), and hence a simple comparison won't
   * work (since things wrap around at 0).  e.g. the time 1 is (usually) later
   * than the time 0xffffffff (2 ticks later), even though in unsigned
   * arithmetic 0xffffffff is larger than 1.
   *
   * We use the (reasonable - I think so anyway) convention that if the
   * difference (t1 - t2) is less than half a DWORD (i.e. 0x80000000) then
   * t1 is later, otherwise it is earlier.
   */

  if ((t1 - t2) & 0x80000000)
    return (1);
  return (0);
}
