/* Various support routines for E614 Slow controls
 *
 * P. W. Green - April 2001
 */

#include <stdio.h>
//#include <stdarg.h>
//#include <ctype.h>

#include "midas.h"

#include "e614proto.h"

extern char *frontend_name;

INT my_find_and_load (HNDLE hDB, HNDLE hRoot, const char *name, HNDLE *hKey, void *data, int numvalues, int type)
{
  /* function to locate the Key for data and load it from the odb
   * hKey may be null if the key is not required
   * Returns SUCCESS if successful
   */
  HNDLE hTemp;
  int size;
  int status;
  
  status = db_find_key (hDB, hRoot, name, &hTemp);
  if (status != SUCCESS)
    {
      cm_msg (MERROR, frontend_name, "Cannot find \'%s\', error %d", name, status);
      return status;
    }

  if (type == TID_BYTE || type == TID_SBYTE || type == TID_CHAR || type == TID_STRING)
    size = numvalues;
  else if (type == TID_WORD || type == TID_SHORT)
    size = numvalues * sizeof (short);
  else if (type == TID_DWORD || type == TID_INT
	   || type == TID_BOOL  || type == TID_FLOAT)
    size = numvalues * sizeof (DWORD);
  else if (type == TID_DOUBLE)
    size = numvalues * sizeof (double);
  else
    {
      cm_msg (MERROR, frontend_name, "Key \'%s\' unknown type %d", name, type);
      return FE_ERR_ODB;
    }

  status = db_get_data (hDB, hTemp, data, &size, type);
  if (status != SUCCESS)
    {
      cm_msg (MERROR, frontend_name, "Cannot read value of \'%s\', error %d", name, status);
      return status;
    }

  if (hKey != NULL)
    *hKey = hTemp;

  return SUCCESS;
}

/* end file */
