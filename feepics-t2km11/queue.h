/* Definitions required for queue management
 */

#include "midas.h"

#ifndef QUEUE_H_INCLUDED
#define QUEUE_H_INCLUDED

struct quentry {
  struct quentry *next;    /* Pointer to next */
  DWORD time;              /* Time it is to be executed */
  INT Period;              /* Period */
  WORD eventid;            /* Allow to change Event ID */
  WORD triggermask;        /* And Trigger Mask */
  int function;            /* function to perform */
  int address;             /* Address to talk to */
  union {
    int iparam;            /* Parameter (for write) */
    float fparam;
    double dparam;
    void*  pparam;
  } param;
  int stuff;               /* We clearly have a bug somewhere.  If I don't leave space
			      it causes the HV guy to crash when I re-queue 
			      after ramping!
			   */
  /* (Possibly more stuff here later) */
};

struct queue {
  struct quentry *head;
  struct quentry *tail;
  DWORD num;
};

/* Some special Functions
 */
#define QFN_DELETE 0
#define QFN_HEARTBEAT 1
#define QFN_EVGEN 2
#define QFN_STATUS 3
#define QFN_UPDATEVALUE 4

/* Functions specific to particular FEs
 */

#define QFN_HPSCAN   11

#define QFN_HVMON 21
#define QFN_HVSET 22
#define QFN_HVOFF 23
#define QFN_HEFLOW 24
#define QFN_CO2FLOW 25

#define QFN_PBMON 31

#define QFN_PAMON 41
#define QFN_PASETVTP 42
#define QFN_PASETTHRESH 43
#define QFN_PASETTPE 44
#define QFN_PASETALL 45
#define QFN_PAMONVTH 46
#define QFN_PAMONVTP 47
#define QFN_PAMONCTRL 48
#define QFN_PAMONTPE 49
#define QFN_PAMONMAXTEMP 50

#define QFN_SCREAD 61

#define QFN_EPICSREAD 71

#define QFN_LASREAD   81

#define QFN_NMRREAD   81

/* Function prototypes
 */

struct quentry *popqueue (struct queue *pq);
void insqueue (struct quentry *pe, struct queue *pq);
void instail (struct quentry *pe, struct queue *pq);
int isbefore (DWORD t1, DWORD t2);

#endif
