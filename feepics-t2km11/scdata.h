/*
 * Name: scdata.h
 * Author: K.Olchanski
 * Date: 29 Aug 2002
 * Description: slow controls data object
 *
 * $Id: scdata.h,v 1.12 2006/05/12 02:25:43 twistonl Exp $
*/

#ifndef SCDATA_H
#define SCDATA_H

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include <time.h>

//#include "ybos.h"

//extern char* frontend_name;

typedef struct
{
  const char* fName;      /* data name */
  HNDLE       fDbValuesHandle; /* database handle for values */
  HNDLE       fDbStatusHandle; /* database handle for status */
  int         fSize;      /* data array size */
  int         fEventBase; /* base value for event number */
  float*      fValues;    /* calibrated data values */
  float*      fRawValues; /* uncalibrated (raw) data values */
  DWORD*      fStatus;    /* data status */
  time_t*     fTimestamp; /* data time stamp */
} ScData_t;

int WriteScValues(HNDLE dbHandle,const ScData_t* sc);

HNDLE GetHandle(HNDLE hDB,HNDLE dir,const char *name,int type)
{
  /* Find key in the ODB */
  HNDLE key;
  int status = db_find_key (hDB, dir, (char*)name, &key);

  if (type != 0 && status == DB_NO_KEY)
    {
      // create key if it does not exist
      status = db_create_key(hDB, dir, (char*)name, type);
      if (status != SUCCESS)
	{
	  cm_msg (MERROR, frontend_name, "Cannot create \'%s\', error %d", name, status);
	  abort();
	  // NOT REACHED
	  return 0;
	}
      status = db_find_key(hDB, dir, (char*)name, &key);
      cm_msg (MERROR, frontend_name, "Created ODB key \'%s\' of type %d", name, type);
    }

  if (status != SUCCESS)
    {
      if (dir==0)
	cm_msg (MERROR, frontend_name, "Cannot find odb handle for \'%s\', error %d", name, status);
      else
	cm_msg (MERROR, frontend_name, "Cannot find odb handle for %d/\'%s\', error %d", dir, name, status);
      abort();
      // NOT REACHED
      return 0;
    }
  return key;
}

ScData_t *NewScData(HNDLE dbHandle,HNDLE variablesHandle,const char* statusName,const char*name,int size,float defaultValue,int eventBase)
{
  int status;
  ScData_t *sc;
  char keyName[256];
  int keyType;
  int keyNumValues;
  int keyItemSize;
  int i;
  int size1;

  sc = (ScData_t*)calloc(1,sizeof(ScData_t));
  assert(sc!=NULL);

  sc->fName = name;
  sc->fEventBase = eventBase;

  sc->fDbValuesHandle = 0;
  if (statusName)
    sc->fDbStatusHandle = GetHandle(dbHandle,0,statusName,TID_DWORD);
  else
    sc->fDbStatusHandle = 0;

  sc->fSize   = size;
  sc->fValues = (float*)calloc(size,sizeof(sc->fValues[0]));
  assert(sc->fValues != NULL);

  sc->fRawValues = (float*)calloc(size,sizeof(sc->fRawValues[0]));
  assert(sc->fRawValues != NULL);

  sc->fStatus = (DWORD*)calloc(size,sizeof(sc->fStatus[0]));
  assert(sc->fStatus != NULL);

  sc->fTimestamp = (time_t*)calloc(size,sizeof(sc->fTimestamp[0]));
  assert(sc->fTimestamp != NULL);

  if (variablesHandle)
    {
      status = db_find_key (dbHandle, variablesHandle, (char*)name, &sc->fDbValuesHandle);
      if (status == DB_NO_KEY)
	{
	  // create key if it does not exist
	  status = db_create_key(dbHandle, variablesHandle, (char*)name, TID_FLOAT);
	  if (status != SUCCESS)
	    {
	      cm_msg (MERROR, frontend_name, "Cannot create \'%s\', error %d", name, status);
	      abort();
	      // NOT REACHED
	      return 0;
	    }

	  status = db_find_key (dbHandle, variablesHandle, (char*)name, &sc->fDbValuesHandle);
	  cm_msg (MERROR, frontend_name, "Created ODB key \'%s\' of type %d", name, TID_FLOAT);

	  WriteScValues(dbHandle, sc);
	}

      if (status != SUCCESS)
	{
	  cm_msg (MERROR,frontend_name,"Can't get DB handle for Variables/%s, error %d",name,status);
	  abort();
	  return 0;
	}

      status = db_get_key_info(dbHandle,sc->fDbValuesHandle,keyName,sizeof(keyName)-1,&keyType,&keyNumValues,&keyItemSize);
      if (status != SUCCESS)
	{
	  cm_msg (MERROR,frontend_name,"Can't get key info for \'%s\', error %d",name,status);
	  abort();
	  return 0;
	}
      
      if (keyType != TID_FLOAT)
	{
	  cm_msg (MERROR,frontend_name,"Key \'%s\' type is %d, not %d (TID_FLOAT)",name,keyType,TID_FLOAT);
	  abort();
	  return 0;
	}
      
      if (keyItemSize != 4) /* sizeof(TID_FLOAT) is defined to be 4 */
	{
	  cm_msg (MERROR,frontend_name,"Key \'%s\' wrong item size %d should be 4",name,keyItemSize);
	  abort();
	  return 0;
	}
      
      if (keyNumValues < size)
	{
	  cm_msg (MERROR,frontend_name,"Key \'%s\' is too short: size %d should be at least %d",name,keyNumValues,size);
	  abort();
	  return 0;
	}
      
      if (keyNumValues > size)
	{
	  cm_msg (MINFO,frontend_name,"Key \'%s\' is longer than expected: size %d expected %d",name,keyNumValues,size);
	  abort();
	  return 0;
	}
    }

  size1 = sizeof(float);
  for (i=0; i<size; i++)
    {
      sc->fStatus[i] = ST_UNKNOWN;

      if (sc->fDbValuesHandle)
	{
	  status = db_get_data_index(dbHandle,sc->fDbValuesHandle,&sc->fValues[i],&size1,i,TID_FLOAT);
	  if (status != SUCCESS)
	    {
	      cm_msg (MERROR,frontend_name,"Error %d getting value %d for \'%s\' from odb",status,i,name);
	      sc->fValues[i] = defaultValue;
	    }
	}
      else
	sc->fValues[i] = defaultValue;
    }

#if 0
  printf("key [%s] is [%s], type %d, numVaues: %d, itemSize: %d\n",name,keyName,keyType,keyNumValues,keyItemSize);
  for (i=0; i<size; i++)
    {
      printf(" %f",sc->fValues[i]);
    }
  printf("\n");
#endif

  return sc;
}

void DeleteScData(ScData_t** ptr)
{
  if (ptr == NULL) return;
  free((*ptr)->fValues);
  free((*ptr)->fRawValues);
  free((*ptr)->fStatus);
  free((*ptr)->fTimestamp);
  (*ptr)->fName   = 0;
  (*ptr)->fValues = 0;
  (*ptr)->fRawValues = 0;
  (*ptr)->fStatus = 0;
  (*ptr)->fTimestamp = 0;
  free(*ptr);
  *ptr = 0;
}

int ReadScValues(HNDLE dbHandle,ScData_t* sc)
{
  int size;
  int status;

  if (sc->fDbValuesHandle == 0)
    return ST_OK;

  size = sc->fSize * sizeof (float);
  status = db_get_data (dbHandle,sc->fDbValuesHandle,sc->fValues,&size,TID_FLOAT);
  if (status != SUCCESS)
    cm_msg (MERROR,frontend_name,"Error %d reading \'%s\' values",status,sc->fName);

  return status;
}

int WriteScValues(HNDLE dbHandle,const ScData_t* sc)
{
  int size;
  int status;

  if (sc->fDbValuesHandle == 0)
    return ST_OK;

  size = sc->fSize * sizeof (float);
  status = db_set_data (dbHandle,sc->fDbValuesHandle,sc->fValues,size,sc->fSize,TID_FLOAT);
  if (status != SUCCESS)
    cm_msg (MERROR,frontend_name,"Error %d writing \'%s\' values",status,sc->fName);

  return status;
}

int WriteScStatus(HNDLE dbHandle,const ScData_t* sc)
{
  int size;
  int status;

  if (sc->fDbStatusHandle == 0)
    return ST_OK;

  size = sc->fSize * sizeof(DWORD);
  status = db_set_data (dbHandle, sc->fDbStatusHandle, sc->fStatus, size, sc->fSize, TID_DWORD);
  if (status != SUCCESS)
    cm_msg (MERROR,frontend_name,"Error %d writing \'%s\' status",status,sc->fName);

  return status;
}

float GetScValue(ScData_t* sc,int channel)
{
  assert((channel>=0)&&(channel<sc->fSize));
  return sc->fValues[channel];
}

float GetScRawValue(ScData_t* sc,int channel)
{
  assert((channel>=0)&&(channel<sc->fSize));
  return sc->fRawValues[channel];
}

void SetScValue(ScData_t* sc,int channel,float rawvalue,float value)
{
  assert((channel>=0)&&(channel<sc->fSize));
  sc->fRawValues[channel] = rawvalue;
  sc->fValues[channel] = value;
  time(&(sc->fTimestamp[channel]));
}

void SetScStatus(ScData_t* sc,int channel,DWORD status,const char*fmt,...)
{
  assert((channel>=0)&&(channel<sc->fSize));
  if (status != sc->fStatus[channel])
    {
      char buf[256];
      va_list ap;
      va_start(ap,fmt);
      vsnprintf(buf,sizeof(buf)-1,fmt,ap);
      va_end(ap);
      printf("%s[%d] status change: %d->%d, value %f, %s\n",sc->fName,channel,(int)sc->fStatus[channel],(int)status,sc->fValues[channel],buf);
      sc->fStatus[channel] = status;
    }
}

int FloatToInt(float fvalue)
{
  int ivalue;
  memcpy(&ivalue,&fvalue,4);
  return ivalue;
}

float IntToFloat(int ivalue)
{
  float fvalue;
  memcpy(&fvalue,&ivalue,4);
  return fvalue;
}

#if 0
void ScYbosEventEVID(int runno,DWORD*pevent)
{
  DWORD *pdata;
  WORD *pdata16;

  ybk_create(pevent, "EVID", I4_BKTYPE, &pdata);

  pdata16 = (WORD*)pdata;
  *pdata16++ = 0;                /*trigger mask*/
  *pdata16++ = EVENT_ID(pevent); /* event ID */
  pdata = (DWORD*)pdata16;
  *pdata++ = SERIAL_NUMBER(pevent);     /* Serial number */
  *pdata++ = TIME_STAMP(pevent);        /* Time Stamp */
  *pdata++ = runno;                     /* run number */
  *pdata++ = *((DWORD *)frontend_name); /* frontend name */

  ybk_close(pevent, pdata);
}

int ScYbosEvent(int runno,ScData_t* sc,int channel,DWORD* pevent)
{
  int eventNumber;
  DWORD *pdata;
#if 0
  static int count = 0;
  float test1,test3;
  int test2;
#endif

  assert((channel>=0)&&(channel<sc->fSize));

  eventNumber = sc->fEventBase + channel;

  // do not generate events with stale data
  if (sc->fStatus[channel] == ST_UNKNOWN)
    {
#if 0
      printf("ScYbosEvent: event %d, name %s, channel %d, status %d, stale data, no event.\n",
	     eventNumber,
	     sc->fName,
	     channel,
	     (int)sc->fStatus[channel]);
#endif
      return 0;
    }

#if 0
  printf("ScYbosEvent: event %d, name %s, channel %d, status %d, value %f\n",
	 eventNumber,
	 sc->fName,
	 channel,
	 (int)sc->fStatus[channel],
	 sc->fValues[channel]);
#endif

#if 0
  if (count > 0)
    return 0;

  count ++;

  test1 = 123.456;
  test2 = FloatToInt(test1);
  test3 = IntToFloat(test2);
  printf("test: %f %d %f\n",test1,test2,test3);
#endif

  TRIGGER_MASK(pevent) = eventNumber;

  ybk_init(pevent);

  ScYbosEventEVID(runno,pevent);

  ybk_create(pevent, "SLOW", I4_BKTYPE, &pdata);

  *pdata++ = eventNumber;
  *pdata++ = sc->fTimestamp[channel];
  *pdata++ = sc->fStatus[channel];
  *pdata++ = FloatToInt(sc->fRawValues[channel]);
  *pdata++ = FloatToInt(sc->fValues[channel]);

  ybk_close(pevent, pdata);
  return (ybk_size(pevent));
}
#endif
#endif
/* end file */
